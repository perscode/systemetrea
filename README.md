# systemetrea.eu

Project's frontend is written in typescript Angular 6+. It uses MongoDB on a NodeJS and Express backend, written in javascript.

## TODOs / missing / coming features

> Database Tests
> User registration and email notifications  
> User option to track articles  
> Improved Admin interface to handle user signups  
> User account view for changing password & email  
> Imroved route handling for articles / article-detail view with Subjects as Observables to support "nicer" URLs  
> User activity Logging  
> Article Rating  