let dotenv = require('dotenv').config({path: './.env'});

if (dotenv.parsed) {
  for (var k in dotenv.parsed) {
    process.env[k] = dotenv.parsed[k];
  }
}
if (!process.env.secret) {
  process.env.secret = 'MY_SECRET';
}

module.exports = dotenv;