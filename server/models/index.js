module.exports = {
  Article: require('./article.model').Article,
  Reduced: require('./reduced.model').Reduced,
  User: require('./user.model')
};
