const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reducedSchema = new Schema(
  {
    count: Number,
    date: { type: Date, required: true, unique: true, dropDups: true },
    typeStatistics: [{ name: String, count: Number }],
    articles: [ { type: Schema.Types.ObjectId, ref: 'Article' }]
  },
  {
    collection: 'reduced',
    read: 'nearest'
  }
);

reducedSchema.set('toObject', { getters: true });

mongoose.model('Reduced', reducedSchema);
