var express = require('express');
var router = express.Router();

const services = require('../services');
const articleService = services.articleService;
const utilityService = services.utilityService;

router.get('/articles', (req, res) => {
  articleService.getArticles(req, res);
});

router.get('/articles/statistics', (req, res, next) => {
  articleService.getStatistics(req, res);
});

router.get('/articles/:id', (req, res) => {
  articleService.getArticle(req, res);
});

module.exports = router;
