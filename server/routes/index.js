require('../models/mongo');
var express = require('express');
var ExpressBrute = require('express-brute');
var store = new ExpressBrute.MemoryStore();
var bruteforce = new ExpressBrute(store);

var jwt = require('express-jwt');
var auth = jwt({
  secret: process.env.secret,
  userProperty: 'payload'
}).unless({path: ['/api/login', '/api/register']});

var router = express.Router();

router.use('/', auth, require('./reduced.routes'));
router.use('/', auth, require('./article.routes'));
router.use('/', bruteforce.prevent, auth, require('./auth.routes').router);

module.exports = router;

