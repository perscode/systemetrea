var express = require('express');
var router = express.Router();

const services = require('../services');
const utilityService = services.utilityService;

router.get('/reduced/dates', (req, res, next) => {
  utilityService.getReducedDates(req, res);
});

module.exports = router;