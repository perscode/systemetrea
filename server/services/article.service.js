const mongoose = require('mongoose');
const Article = mongoose.model('Article');
const Reduced = mongoose.model('Reduced');
const reducedDB = require('./db/reduced');
const articleDB = require('./db/article');

function getArticles(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : 50;
  const offset = req.query.offset ? parseInt(req.query.offset) : 0;
  const date = req.query.date;
  const varugrupp = req.query.varugrupp;
  const query = {
    date: date
  };
  const populate = {
    path: 'articles',
    select: 'namn namn2 artikelid varugrupp argang prisinklmoms prissanktProcent lastModified nr tidigarePris',
    options: {
      sort: { 'prissanktProcent': 'descending' },
      limit: limit,
      skip: offset
    }
  };
  if (req.query.varugrupp) {
    populate.match = { varugrupp: {'$regex': varugrupp, $options:'i'} };
  }
  const docquery = Reduced
    .find(query)
    .populate(populate)
    .sort({ 'date': 'descending' });
  reducedDB.getReducedArticles(docquery).then(articles => {
    res.status(200).json(articles);
  })
  .catch(error => res.status(500).send(error));
}

function getArticle(req, res) {
  const id = req.params.id;
  const docquery = Article
    .findOne({ artikelid: id });
  articleDB.getArticle(docquery).then(article => {
    res.status(200).json(article);
  })
  .catch(error => res.status(500).send(error));
};

const getStatistics = async (req, res) => {
  const docquery = Reduced
    .findOne({'articles.0': { '$exists': true } })
    .sort({ 'date': 'descending' })
    .limit(1)
    .select('typeStatistics')
    .populate({
      path: 'articles',
      select: 'namn namn2 artikelid varugrupp prisinklmoms prissanktProcent tidigarePris'
    })
    .lean();
  reducedDB.getStatistics(docquery).then(docs => {
    const statisticsResponse = mergeStatistics(docs);
    res.status(200).json(statisticsResponse);
  }).catch(err => {
    console.log('err', err);
    res.status(500).json(err);
  });
}

const mergeStatistics = (docs) => {
  const all = {};
  docs.typeStatistics.forEach(type => {
    const name = type.name;
    if (!all[name])  all[name] = { count: 0, articles: [] };
    all[name].count += type.count;
  });
  docs.articles.forEach(article => {
    all[article.varugrupp].articles.push(article);
  });
  const list = [];
  for (let key in all) {
    list.push({
      type: key,
      count: all[key].count,
      articles: all[key].articles.map(article => {
        const bubbleChartItem = {
          label: [`${article.namn} ${article.namn2}`],
          backgroundColor: getCssColor(key),
          borderColor: '#000',
          data: [{
            x: article.tidigarePris,
            y: article.prisinklmoms,
            r: article.prissanktProcent > 1 ? article.prissanktProcent : 1
          }]
        };
        return bubbleChartItem;
      })
    });
  }
  return list;
}

const getCssColor = (type) => {
  let color;
  switch (type) {
    case 'Alkoholfritt': color = 'rgba(255,255,255)'; break;
    case 'Cider': color = 'rgba(173, 255, 47)'; break;
    case 'Gin': color = 'rgba(234, 237, 237)'; break;
    case 'Mousserande vin': color = 'rgba(236,205,19)'; break;
    case 'Okryddad sprit': color = 'rgba(0, 0, 255)'; break;
    case 'Portvin': color = 'rgba(114, 47, 55)'; break;
    case 'Rom': color = 'rgba(113, 102, 117)'; break;
    case 'Rosévin': color = 'rgba(219, 177, 205)'; break;
    case 'Rött vin': color = 'rgba(116, 47, 55, 1)'; break;
    case 'Tequila och Mezcal': color = 'rgba(245, 203, 165)'; break;
    case 'Vin av flera typer': color = 'rgba(0, 153, 153)'; break;
    case 'Vitt vin': color = 'rgba(241,242,133)'; break;
    case 'Whisky': color = 'rgba(206, 142, 104)'; break;
    case 'Öl': color = 'rgba(255, 204, 0)'; break;
    case 'Övrigt starkvin': color = 'rgba(153, 0, 0)'; break;
  }
  return color;
}
module.exports = {
  getArticles,
  getArticle,
  getStatistics
}














