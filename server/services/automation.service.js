
const schedule = require('node-schedule');
const chalk = require('chalk');
const async = require('async');
const path = require('path');

const reducedDB = require('./db/reduced');
const compareService = require('./compare.service');
const fileService = require('../utilities/file-service');
const fileDownloader = require('../utilities/file-downloader');
const xmlReaderService = require('../utilities/xml-article-reader');
const DATA_PATH = path.join(process.cwd(), 'server', 'data');

/**
 * Download a new xml from systembolaget.se and compare
 * with current articles in the database.
 */
const downloadAndRunNewComparison = () => {
  fileDownloader.getSortimentfilen(str => {
    fileService.renameFile(str, (err, fileName) => {
      getArticlesFromXml(fileName, (err, xmlArticles) => {
        compareService.compareArticleLists(fileName, (err, result) => {
          console.log('done');
        });
      })
    });
  })
}

/**
 * Runs the xml-files in server/data, adds and compares in sequence
 */
const buildDatabaseFromLocalFiles = async () => {

  console.log(chalk.green('reading files from data folder...'));
  const files = await fileService.readDataDir();
  const nameDateArr = [];
  for (const file of files) {
    if (file.indexOf('.xml') > 0) {
      const date = file.substring(file.indexOf('_') + 1).split('.xml')[0];
      nameDateArr.push({
        date: date,
        name: file
      });
    }
  }
  purgePreviouslyRun(nameDateArr).then(newList => {
    const filteredList = newList;
    if (filteredList && filteredList.length > 0) {
      const fileNames = sortByDate(filteredList);
      console.log(chalk.white(`found ${fileNames.length} .xml files`));
      iterateXmlFilesWithArticles(fileNames, 0);
    } else {
      console.log(`No new files to run`);
    }
  });
}

const purgePreviouslyRun = (files) => {
  return new Promise((resolve, reject) => {
    const dates = [];
    let index = 0;
    files.forEach(file => {
      dates.push(file.date);
    });
    async.eachSeries(dates, (date, next) => {
      reducedDB.hasDate(date).then(exists => {
        if (exists) {
          console.log(`${date} has already been run`);
          files.splice(index, 1);
        } else {
          index++;
        }
        next();
      });
    }, (error) => {
      if (error) {
        return reject(error);
      }
      resolve(files);
    });
  });
}

const iterateXmlFilesWithArticles = (fileNames, index) => {
  if (!fileNames[index]) {
    console.log('no more files to read');
  } else {
    console.log(chalk.green(`loading: ${fileNames[index]}`));
    getArticlesFromXml(fileNames[index], (err, articlesFromXml) => {
      if (err) {
        console.log(`Error processing ${fileNames[index]}\nreason: ${err}`);
        return iterateXmlFilesWithArticles(fileNames, index+1);
      }
      compareService.compareArticleLists(articlesFromXml, (err, result) => {
        iterateXmlFilesWithArticles(fileNames, index+1);
      });
    });
  }
};

const getArticlesFromXml = (fileName, callback) => {
  console.log(chalk.green('-- New database comparison initiated --'));
  const filePath = path.join(DATA_PATH, fileName);
  xmlReaderService.getJSONFromXML(filePath, (err, articlesFromXml) => {
    if (err) {
      return callback(err);
    }
    callback(null, articlesFromXml);
  });
}

const sortByDate = (files) => {
  files.sort((a, b) => {
    return new Date(a.date) - new Date(b.date);
  });
  const fileNames = [];
  files.forEach(o => {
    fileNames.push(o.name);
  });
  return fileNames;
}

const init = () => {
  const rule = new schedule.RecurrenceRule();
  rule.minute = 0;
  rule.hour = 6;
  rule.date = 1;
  schedule.scheduleJob(rule, function () {
    downloadAndRunNewComparison();
  });
  buildDatabaseFromLocalFiles();
  // downloadAndRunNewComparison();
  // compareService.runComparison('sortimentfilen_2016-12-02.xml');
  // compareService.runComparison('sortimentfilen_2017-06-01.xml');
  // compareService.runComparison('sortimentfilen_2017-09-01.xml');
  // compareService.runComparison('sortimentfilen_2017-12-01.xml');
  // compareService.runComparison('sortimentfilen_2018-03-01.xml');
  // compareService.runComparison('sortimentfilen_2018-05-31.xml');
  // compareService.runComparison('sortimentfilen_2018-07-16.xml');
  // compareService.runComparison('sortimentfilen_2018-07-20.xml');
}

module.exports = {
  init
}
