
const path = require('path');
const async = require('async');
const mongoose = require('mongoose');
const chalk = require('chalk');
const Reduced = mongoose.model('Reduced');
const Article = mongoose.model('Article');
const articleDB = require('./db/article');
const log = {};
let articleIdMap;
let typeStatistics;
let reducedPriceList;
let createdDate;

const compareArticleLists = (newArticles, callback) => {
  articleIdMap = {};
  articleDB.getArticles().then(articles => {
    console.log(`${newArticles.length} articles found\n${Object.keys(articles).length} currently in the database`);
    articles.forEach(article => {
      articleIdMap[article.artikelid] = article.toObject();
    });
    typeStatistics = {};
    log.newArticlesCount = 0;
    log.pricedChangedCount = 0;
    log.reducedPricesCount = 0;
    reducedPriceList = [];
    let count = Object.keys(articleIdMap).length;
    const dbEmpty = count === 0;
    createdDate = newArticles[0].lastModified;
    console.log(chalk.blue('info ') + chalk.white(`xml from ${createdDate} contains`) + `
    ${chalk.green(newArticles.length)} articles`);
    initDatabase(dbEmpty, newArticles).then(() => {
      if (!dbEmpty) {
        let updateQueries = [];
        newArticles.forEach((newArticle) => {
          try {
            let docquery;
            const oldArticle = articleIdMap[newArticle.artikelid];
            // if it's a new article, we want to save it to the collection.
            if (!oldArticle) {
              const article = new Article(newArticle);
              article.save(err => {
                if (err) {
                  console.log('err saving article', err);
                } else {
                  log.newArticlesCount++;
                }
              });
            } else { // Article nr exists in DB.
              docquery = getUpdateQuery(oldArticle, newArticle);
              updateQueries.push(docquery);
            }
          } catch (err) {
            console.log(err);
          }
        });
        resolveQueries(updateQueries).then(() => {
          resolvePriceChanges(reducedPriceList).then((reducedCount) => {
            if (reducedCount) {
              // TODO move&store sortimentfilen_*date*.xml in data/saved
              log.reducedPricesCount = reducedCount;
            }
            printLog();
            callback();
          }).catch(err => {
            callback(`err resolving priceChanges`, err);
          });
        }).catch(err => {
          callback(`error resolveLists(): ${err}`);
        });
      } else {
        console.log('Database was empty. Added ', newArticles.length, 'articles');
        resolvePriceChanges([]).then(() => {
          callback(null);
        }).catch(error=> {
          callback(error);
        });
      }
    }).catch(err => {
      callback(`error on initDB: ${err}`);
    });
  });
}

const getUpdateQuery = (oldArticle, newArticle) => {
  const priceReduced = oldArticle.prisinklmoms > newArticle.prisinklmoms;
  const priceIncreased = oldArticle.prisinklmoms < newArticle.prisinklmoms;
  const query = { _id: oldArticle._id };
  let fields = {};
  if (priceReduced || priceIncreased) {
    fields = getUpdatedPriceAttributes(oldArticle, newArticle);
  }
  if ((oldArticle.namn !== newArticle.namn) || (oldArticle.namn2 !== newArticle.namn2)) {
    fields.namn = newArticle.namn;
    fields.namn2 = newArticle.namn2;
  }

  fields.lastModified = newArticle.lastModified;
  const docquery = Article.findOneAndUpdate(
    query,
    { $set: fields },
    { new: true }
  );
  return docquery;
}

const getUpdatedPriceAttributes = (oldArticle, newArticle) => {
  log.pricedChangedCount++;
  let tidigarePris = null;
  if (oldArticle.prisinklmoms > newArticle.prisinklmoms) {
    tidigarePris = oldArticle.prisinklmoms;
    typeStatistics[newArticle.varugrupp] = typeStatistics[newArticle.varugrupp] ? typeStatistics[newArticle.varugrupp]+1 : 1;
    reducedPriceList.push(oldArticle._id);
  }
  oldArticle.prisHistorik.push({ pris: oldArticle.prisinklmoms, datum: oldArticle.lastModified });
  const prisProcent = parseFloat(((100 * (1 - ((newArticle.prisinklmoms) / (oldArticle.prisinklmoms)))).toFixed(1)));
  let fields = {
    prisHistorik: oldArticle.prisHistorik,
    prisinklmoms: newArticle.prisinklmoms,
    prisPerLiter: newArticle.prisPerLiter,
    prissanktProcent: prisProcent
  }
  if (tidigarePris) {
    fields.tidigarePris = tidigarePris;
  }
  return fields;
}

const resolveQueries = (articleQueries) => {
  return new Promise((resolve, reject) => {
    async.eachSeries(articleQueries, (doccquery, next) => {
      doccquery.exec().then(doc => {
        next();
      }).catch((err) => {
        console.log('unable to update article', err);
        next(err);
      });
    }, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

const resolvePriceChanges = (articleIds) => {
  const promise = new Promise((resolve, reject) => {
    const statisticsArray = [];
    for (let key in typeStatistics) {
      statisticsArray.push(
        {
          name: key,
          count: typeStatistics[key]
        }
      );
    }
    const originalReduced = {
      count: articleIds.length || 0,
      date: createdDate,
      typeStatistics: statisticsArray || [],
      articles: articleIds || []
    };
    console.log('originalReduced', originalReduced);
    const reduced = new Reduced(originalReduced);
    reduced.save(err => {
      if (err) {
        console.log('err saving reduced articles', err);
        reject(err);
      } else {
        resolve(originalReduced.articles.length);
      }
    });
  });
  return promise;
}

const initDatabase = (dbEmpty, articles) => {
  return new Promise((resolve, reject) => {
    if (dbEmpty) {
      articles.forEach(article => {
        article.prisinklmoms = parseFloat(article.prisinklmoms);
        article.prisPerLiter = parseFloat(article.prisPerLiter);
      });
      Article.insertMany(articles, (error, docs) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    } else {
      resolve();
    }
  });
}

const printLog = () => {
  console.log(chalk.blue('info ') + chalk.white(' results...') + `
  # of new articles: ${chalk.green(`${log.newArticlesCount}`)}
  # of modified articles: ${chalk.green(`${log.pricedChangedCount}`)}
  # of articles reduced in price: ${chalk.green(`${log.reducedPricesCount}`)}
  `);
}

module.exports = {
  compareArticleLists
}