const mongoose = require('mongoose');
const Article = mongoose.model('Article');

const getArticle = (query) => {
  return new Promise((resolve, reject) => {
    query.exec().then(article => {
      console.log('article', article);
      resolve(article);
    }).catch(err => {
      console.log('err fetching article', err);
      reject(err);
    });
  });
}

const getArticles = () => {
  return new Promise((resolve, reject) => {
    Article.find().then(res => {
      resolve(res);
    });
  });
}

module.exports = {
  getArticle,
  getArticles
}
