const mongoose = require('mongoose');
const Reduced = mongoose.model('Reduced');

const getReducedArticles = (query) => {
  return new Promise((resolve, reject) => {
    if (!query) {
      return reject('missing query');
    }
    query.exec()
    .then(doc => {
      resolve(doc[0]);
    })
    .catch(error => reject(error));
  });
}

async function hasDate(date) {
  const docquery = Reduced.find({date: date}).limit(1);
  return await docquery.exec().then(doc => {
    return doc[0] ? true : false;
  });
}

const getReducedDates = (fields) => {
  return new Promise((resolve, reject) => {
    const select = fields ? fields : 'date';
    const docquery = Reduced
      .find({'articles.0': { '$exists': true } }).sort({ 'date': 'descending' }).select(select);

    docquery.exec()
      .then(doc => {
        resolve(doc);
      })
      .catch(error => reject(error));
  });
}

const getStatistics = async (docquery) => {
  return new Promise((resolve, reject) => {
    docquery.exec().then(doc => {
      resolve(doc);
    }).catch(err => {
      reject(err);
    });
  })
}

module.exports = {
  getReducedArticles,
  getReducedDates,
  getStatistics,
  hasDate
}
