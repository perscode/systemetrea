
module.exports = {
  articleService: require('./article.service'),
  utilityService: require('./utility.service'),
  userService: require('./user.service')
};
