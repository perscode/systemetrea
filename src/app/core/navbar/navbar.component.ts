import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UserProfile } from '../models';
declare var UIkit: any;

class MenuItem {
  constructor(public caption: string, public link: any[], public hide?: boolean) { }
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit, OnDestroy {
  // loggedInSub: Subscription;
  // loggedIn: boolean;
  // isAdminSub: Subscription;
  isAdmin: boolean;
  subscription: Subscription;
  profile: UserProfile;
  sideBar: any;
  menuItems: MenuItem[];

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.sideBar = UIkit.offcanvas('#profile', {
      mode: 'reveal',
      overlay: true,
      flip: true
    });
    this.subscription = this.authService.userProfile.subscribe(
      profile => {
        if (!profile) {
          this.router.navigate(['/login']);
        } else {
        this.profile = profile;
        this.isAdmin = profile.admin;
        }
      });
  }

  showSidebar(state) {
    this.sideBar.show();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  isLoggedIn(): boolean {
    return this.authService.authenticated;
  }

  logout() {
    this.authService.logout();
    this.sideBar.hide();
  }
}
